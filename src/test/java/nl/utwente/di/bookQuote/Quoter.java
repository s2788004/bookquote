package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    HashMap<String, Double> prices = new HashMap<String, Double>();

    public Quoter() {
        prices.put("1", 10.00);
        prices.put("2", 45.00);
        prices.put("3", 20.00);
        prices.put("4", 35.00);
        prices.put("5", 50.00);
    }

    double getBookPrice (String isbn) {
        if (prices.containsKey(isbn)) {
            return prices.get(isbn);
        }
        return 0.00;
    }
}
